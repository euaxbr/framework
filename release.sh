rm -rf build
rm -rf dist
rm -rf rptf.egg-info

pipenv run python setup.py sdist bdist_wheel
TWINE_USERNAME=__token__ TWINE_PASSWORD=$PYPI_TOKEN pipenv run twine upload --non-interactive --repository pypi dist/*
